#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Pull latest
git -C "$DIR" pull

##### DEPENDENCIES ######
if [ ! -d "$DIR/hdt-java/" ]; then
    echo "" # git clone https://gitlab.com/wurssb/hdt-java.git $DIR/hdt-java
fi

if [ ! -d "$DIR/EmpusaCodeGen/" ]; then
	git clone https://gitlab.com/Empusa/EmpusaCodeGen $DIR/EmpusaCodeGen
fi

if [ ! -d "$DIR/OWLApi/" ]; then
	git clone https://gitlab.com/Empusa/OWLApi $DIR/OWLApi
fi

if [ ! -d "$DIR/RDFSimpleCon/" ]; then
	git clone https://gitlab.com/Empusa/RDFSimpleCon $DIR/RDFSimpleCon
fi

if [ ! -d "$DIR/ShexApi/" ]; then
	git clone https://gitlab.com/Empusa/ShexApi $DIR/ShexApi
fi

echo "Installing"
# $DIR/hdt-java/install.sh # Is performed through RDFSimplecon
$DIR/ShexApi/install.sh
$DIR/RDFSimpleCon/install.sh
$DIR/OWLApi/install.sh
$DIR/EmpusaCodeGen/install.sh
echo "Installation finished"
